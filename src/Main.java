import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/*
    Esta aplicación lanza 10 procesos que llaman a lenguaje.jar pasándole 2 argumentos o no.
 */
public class Main {
    public static void main(String[] args) {

        Process proceso; //Declaramos el proceso.
        String nombreFichero = "";
        File archivo = null;
        RandomAccessFile raf = null;
        int r; // El número de palabras que se van a generar.

        // Con este if se comprueban si se pasan argumentos.
        if (args.length > 0){
            r = Integer.parseInt(args[0]);
            nombreFichero = args[1];
        } else {
            r = 40;
            nombreFichero = "miFicheroLenguaje.txt";
        }

        archivo = new File(nombreFichero);
        // Comprobamos si el fichero existe.
        if(!archivo.exists()){
            try{
                archivo.createNewFile(); // Se crea el fichero.
                raf = new RandomAccessFile(archivo, "rw"); // Lo abrimos con permisos de lectura/escritura.
                System.out.println("Creado el archivo "+nombreFichero);
            }catch (Exception e){
                System.err.println("Error al crear el fichero "+ e);
            }finally {
                try{ // Nos aseguramos de cerrar el fichero.
                    if(null != raf){
                        raf.close();
                    }
                }catch (Exception e){
                    System.err.println("Error al cerrar el fichero "+e);
                    System.exit(1);
                }
            }
        }
        // Creamos los 10 procesos.
        try{
            for(int i = 0; i < 10; i++){
                // Con estos strings se generan los argumentos para iniciar los procesos.
                String o="java";
                String u="-jar";
                String y="lenguaje.jar";
                String t=Integer.toString(r);
                String e=nombreFichero;
                String cmd[] = {o,u,y,t,e};
                ProcessBuilder pb = new ProcessBuilder(cmd); // Se guarda el string en el proceso.
                proceso = pb.start(); // Se inicia el proceso.
                System.out.println("Creando el proceso número: "+i);
            }
        }catch (SecurityException se){
            System.err.println("Error de seguridad."+
                    "No se ha podido crear el proceso por falta o error en los permisos");
            se.printStackTrace();
        }catch (IOException ioe){
            System.err.println("Error en el metodo exec().");
            ioe.printStackTrace();
        }catch (Exception e){
            System.err.println("Error, descripcion: "+e.toString());
        }

    }
}